from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd

df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder_unfiltered.csv')

app = Dash(__name__)

app.layout = html.Div([
    html.H1(children='Title of Dash App', style={'textAlign':'center'}),
    dcc.Dropdown(df.country.unique(), 'Canada', id='dropdown-selection'),
    dcc.Graph(id='graph-content'),
    dcc.Graph(id='graph-country'),
    dcc.Slider(1957, 2007, 1, value=2000, id='my-range-slider')
                    ])

@callback(
    Output('graph-content', 'figure'),
    Input('dropdown-selection', 'value'))
def update_graph(value):
    dff = df[df.country == value]
    return px.line(dff, x='year', y='pop')

@callback(
    Output('graph-country', 'figure'),
    Input('my-range-slider', 'value'))
def year(value):
    of_the_year = df[df.year == value]
    return px.bar(of_the_year, x='country', y='pop')

if __name__ == '__main__':
    app.run(debug=True)